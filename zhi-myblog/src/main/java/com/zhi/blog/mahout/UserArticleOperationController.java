package com.zhi.blog.mahout;


import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.collection.CollectionUtil;
import com.zhi.blog.domain.Article;
import com.zhi.blog.mapper.ArticleMapper;
import com.zhi.common.core.domain.R;
import org.apache.mahout.cf.taste.common.TasteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hwp
 * @since 2023-04-12
 */
@RestController
@RequestMapping("/userArticleOperation")
public class UserArticleOperationController {

    @Autowired(required = false)
    private  ArticleMapper articleMapper;
    @Autowired
    UserArticleOperationServiceImpl userArticleOperationService;

    @SaIgnore
    @GetMapping("/recommendations/{userid}")
    public R<List<Article>> recommend(@PathVariable Integer userid) throws TasteException {
        List<Long> recommend = userArticleOperationService.recommend(userid);
        if(CollectionUtil.isNotEmpty(recommend)){
            List<Article> GuestYouLikeArticleList = articleMapper.selectBatchIds(recommend);
            GuestYouLikeArticleList.forEach(a->a.setArticleCover(articleMapper.ImgUrl(a.getArticleCover())));
            return R.ok(GuestYouLikeArticleList);
        }
        return R.ok(Collections.emptyList());
    }

}
