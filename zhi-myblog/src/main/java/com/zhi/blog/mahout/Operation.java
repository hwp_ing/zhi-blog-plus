package com.zhi.blog.mahout;
/**
 * <p>
 *
 * </p>
 *
 * @author hwp
 * @since 2023-04-12
 */
public interface Operation {
    /**
     * 点赞
     */
    Integer LIKE = 1;

    /**
     * 收藏
     */
    Integer COLLECT = 2;
}
