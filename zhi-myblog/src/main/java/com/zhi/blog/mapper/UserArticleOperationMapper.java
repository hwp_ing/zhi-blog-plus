package com.zhi.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhi.blog.mahout.UserArticleOperation;

import java.util.List;

public interface UserArticleOperationMapper extends BaseMapper<UserArticleOperation> {

    List<UserArticleOperation> getAllUserPreference();
}
